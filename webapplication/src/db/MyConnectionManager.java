package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import exception.DAORuntimeException;

public class MyConnectionManager {
    private Connection connection;

    public Connection getConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost/webapplication", "webapplication",
                    "webapplication");
            connection.setAutoCommit(false);
            System.out.println("DB接続完了");
            return connection;
        } catch (SQLException e) {
            throw new DAORuntimeException(e);
        }
    }

    public void closeConnection() {
        try {
            if (connection != null && connection.isClosed() == false) {
                connection.close();
                System.out.println("DB切断完了");
            }
        } catch (SQLException e) {
            throw new DAORuntimeException(e);
        }
    }

    public void commitTransaction() {
        try {
            if (connection != null) {
                connection.commit();
                System.out.println("コミット完了");
            }
        } catch (SQLException e) {
            throw new DAORuntimeException(e);
        }
    }

    public void rollbackTransaction() {
        try {
            if (connection != null) {
                connection.rollback();
                System.out.println("ロールバック完了");
            }
        } catch (SQLException e) {
            throw new DAORuntimeException(e);
        }
    }
}
