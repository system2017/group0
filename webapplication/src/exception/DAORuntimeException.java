package exception;

public class DAORuntimeException extends RuntimeException {

    public DAORuntimeException() {
        super();
    }

    public DAORuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public DAORuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAORuntimeException(String message) {
        super(message);
    }

    public DAORuntimeException(Throwable cause) {
        super(cause);
    }

}
