package exception;

public class ItemSearchException extends Exception {

    public ItemSearchException() {
    }

    public ItemSearchException(String message) {
        super(message);
    }

    public ItemSearchException(Throwable cause) {
        super(cause);
    }

    public ItemSearchException(String message, Throwable cause) {
        super(message, cause);
    }

    public ItemSearchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
